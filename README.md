## LtacDemo
A talk on Ltac programming at University of Waterloo done by me.

Please find pdf distribution in `latex/` folder, and Coq source in `src/` folder.
