(** %\chapter{Low Level Tactics and Proof Terms}% *)

(** I believe that every Coq user would have certain level of understanding of what
_Curry Howard Isomorphism_ is. However, in the context of Coq, its form of
presentation does not really give a concrete feeling of how Curry Howard Isomorphism
gives an impact here. On the other hand, the level of educational materials on Coq
jumps from introductory materials like Software Foundations, to unorthodoxical
materials like %\cite{cpdt}%, to "badly written" technical documentation like
%\cite{refman}%, so this writing is meant to fill in the gap in the hope of making
tactic writing more targetting. It's also arguably important to have a closer look to
proof terms when writing tactics. *)

(** * [change] *)

(* hide *)
(** [change] tactic implements a subsumption rule in Calculus of Inductive
Constructions (CIC c.f. %\cite[Chapter 4]{refman}%), which states two types are
exchangable, if subterms in matching position are the same modulo
$\beta\delta\iota\zeta\eta$ conversions. *)

Section Change.
  Require Import Coq.Lists.List.

  Variable A : Type.
  Hypothesis P : A -> Prop.

  Goal forall l, Forall P l -> Forall P (nil ++ l).
  Proof.
    intros l H.
    match goal with
    | [H : context C [?l] |- _ ] =>
      lazymatch type of l with
      | list A =>
        let tH := type of H in
        let tH' := context C [nil ++ l] in
        change tH with tH' in H
      end
    end.
    exact H.
  Qed.
End Change.

(** [change] is helpful when try to massage terms into a form that matches the form of
certain lemma which is about to be applied. *)

(* unhide *)
(** * [unify] *)

(* hide *)
(** [unify] allows one to write more algorithmic tactics, instead of heuristics, when
dealing with terms with potential existential variables. *)

Section Unify.

  Goal exists l, 2 <= length l /\ Forall (fun n => 1 <= n) l.
  Proof.
    eexists.

    (** We want to perform a bottom-up constructive guess of this [l] should be:

     - we start with [nil];

     - if [nil] doesn't work, we would need to guess a [nat] and [cons] to the existing
       guess, and we start with [O];

     - otherwise we put [S] to existing [nat]. *)
    
    Ltac solve_le := solve [cbv; repeat (apply le_n || apply le_S)].

    Ltac outer l :=
      tryif
        split;
        lazymatch goal with
        | |- _ <= length ?el =>
          is_evar el; unify el l;
          idtac "attempting" l;
          solve_le
        | _ => idtac
        end
      then
        (repeat apply Forall_cons;
         try apply Forall_nil;
         solve_le)
      else
        inner 0 l
    with inner n l :=         
        lazymatch constr:(5) with
        | context[n] =>
          outer (n :: l)
          || inner (S n) l
        end.

    (** Matching a natural number with context is a trick to implement [<=] in raw
    Ltac. *)
    
    Ltac prove := outer (@nil nat).

    prove.
    
    Show Proof.

  (** Indeed, the tactic managed to witness a term that satisfies the specification,
    by constructing it repetitively:
<<
(ex_intro (fun l : list nat => 2 <= length l /\ Forall (fun n : nat => 1 <= n) l)
   (1 :: 1 :: nil)
   (conj (le_n 2)
      (Forall_cons 1 (le_n 1)
         (Forall_cons 1 (le_n 1) (Forall_nil (fun n : nat => 1 <= n))))))

>>

Existential quantification is always difficult, as it asks for a concrete witness on
certain specification. However, if we can write an algorithm that derives the witness,
we can save the burden of explicitly spell out every element of it. *)
    
  Qed.
End Unify.
(* unhide *)

(** * [fix] and [refine] *)

(* hide *)
(** [fix] and [refine] tactics allows the user the spell out the proof term up to a
level.

 Using [refine], we can give the proof term directly. In this case, Coq does nothing
 but type checking and termination checking. *)

Goal forall n, 0 <= n.
Proof.
  refine (fix Hrec (n : nat) :=
            match n with
            | O => le_n 0
            | S n' => le_S 0 n' (Hrec n')
            end).
  
  Restart.

  (** We can also leave holes arbitrarily in [refine], in which case, Coq will try to
  infer as much as possible, and if Coq fails to resolve all the holes, we will be
  entering proof mode, with cases corresponding to the holes. *)

  refine (fix Hrec (n : nat) :=
            match n with
            | O => _
            | S n' => le_S 0 _ _
            end).
  - apply le_n.
  - apply Hrec.

    Show Proof.
    
    (**
<<
(fix Hrec (n : nat) : 0 <= n :=
   match n as n0 return (0 <= n0) with
   | 0 => le_n 0
   | S n' => le_S 0 n' (Hrec n')
   end)
>> *)
Qed.

(** [fix] is a bit higher level. It creates a fixpoint header and put a hole in the
body such that the a fixpoint can be applied in the proof (of course we will need to
make sure the structure is decreasing). *)

Goal forall n, 0 <= n.
Proof.
  fix Hrec 1.
  intro n.
  destruct n.
  - apply le_n.
  - apply le_S. apply (Hrec n).

    Show Proof.
    (**
<<
(fix Hrec (n : nat) : 0 <= n :=
   match n as n0 return (0 <= n0) with
   | 0 => le_n 0
   | S n0 => le_S 0 n0 (Hrec n0)
   end)
>> *)
Qed.

(** It requires some extended [match] in Gallina to rewrite a dependent match body to
finish some easy looking lemmas. But the principle is universal. c.f. %\cite[Chapter
17]{refman}, \cite[Part II, convoy pattern]{cpdt}.% *)

Goal forall A (l : list A), l ++ nil = l.
Proof.
  refine (fix Hrec A l :=
            match l as l0 return l0 ++ nil = l0 with
            | nil => eq_refl
            | x :: xs =>
              (fun (P : forall (l0 : list A), Prop) (ev : P xs) (eq : xs = xs ++ nil) =>
                 match eq in _ = xs0 return P xs0 with
                 | eq_refl => ev
                 end
              ) (fun (l0 : list A) => x :: l0 = x :: xs)
                eq_refl
                (match Hrec A xs in _ = b return b = _ with
                 | eq_refl => eq_refl
                 end)
            end).
  Show Proof.
Qed.
(* unhide *)