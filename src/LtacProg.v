(** %\chapter{Functional Programming in Ltac}% *)

(** Recall that Ltac is in fact a lambda calculus with additional syntax and proof
searching facility. Notably, Ltac is an untyped lambda calculus, which means it's
Turing complete. While most of the time is done on paper, the knowledge from the basic
lambda calculus is not only applicable here, but also the only way to go. *)

(** In lambda calculus, all terms are functions, and the program runs by taking other
functions to apply itself to. In fact, this style is so general that, all functions
can be considered to take a number of callbacks, and one of them might get called when
the functions finishes it's computation, with the results passed as arguments. This is
the well known _CPS_.

 It's very important to realized in Ltac programming that, CPS can sometimes provide
 great help. *)

(** * Programming in CPS *)

(** Consider following programming question: write a Ltac function [gen_reverse], so
that given any Gallina [list], and a [nat] [n], we want to produce a new [list] which
should be the concatenation of the n-partitions of the [list] in reverse order. e.g.

 [gen_reverse 2 (1 :: 2 :: 3 :: 4 :: 5 :: nil)] gives [5 :: 3 :: 4 :: 1 :: 2 ::
 nil].*)

Require Import Coq.Lists.List.

(* hide *)
(** Straightforward recursive implementation as following: *)

Ltac gen_reverse1 n l :=
  lazymatch type of l with
  | list ?T => 
    let rec go n' l acc l' :=
        lazymatch n' with
        | 0 => go n l (@nil T) (rev acc ++ l')
        | S ?n'' =>
          lazymatch l with
          | nil => let x := constr:(rev acc ++ l') in
                  let res := eval cbv in x in res
          | ?x :: ?xs =>
            go n'' xs (x :: acc) l'
          end
        end in
    go n l (@nil T) (@nil T)
  end.

(** CPS implementation as following: *)

Ltac gen_reverse2 n l tac :=
  lazymatch type of l with
  | list ?T => 
    let rec iter n' l acc cb :=
        lazymatch n' with
        | 0 => cb acc l
        | S ?n'' =>
          lazymatch l with
          | nil => cb acc l
          | ?x :: ?xs => iter n'' xs acc ltac:(fun acc l' => cb (x :: acc) l')
          end
        end in
    let rec go n l acc cb :=
        lazymatch l with
        | nil => cb acc
        | _ :: _ => iter n l acc ltac:(fun acc' l' => go n l' acc' cb)
        end in
    go n l (@nil T) ltac:(fun res => tac res)
  end.

Goal True.
  let x := gen_reverse1 2 (1 :: 2 :: 3 :: 4 :: 5 :: nil) in
  idtac x;
    gen_reverse2 2 (1 :: 2 :: 3 :: 4 :: 5 :: nil)
                 ltac:(fun y => match x with y => idtac y; idtac "yes" end).
Abort.

(* hide *)
(** CPS style is observably more complicated, so in which case would it be in flavor?
Consider where the [gen_reverse1] has a bug, introduced by missing a [rev] somewhere.

[[
Ltac gen_reverse1 n l :=
  lazymatch type of l with
  | list ?T => 
    let rec go n' l acc l' :=
        lazymatch n' with
        | 0 => go n l (@nil T) (acc ++ l')
        | S ?n'' =>
          lazymatch l with
          | nil => let x := constr:(acc ++ l') in
                  let res := eval cbv in x in res
          | ?x :: ?xs =>
            go n'' xs (x :: acc) l'
          end
        end in
    go n l (@nil T) (@nil T)
  end.
]]

The above program is entirely wrong. But how can one figure where the bug lies?
Debugger is not quite helpful to be honest. The chances are, [idtac] will be treated
as printf, and therefore one might be tempted to add a [idtac] somewhere to print out
the value. 

[[
Ltac gen_reverse1 n l :=
  lazymatch type of l with
  | list ?T => 
    let rec go n' l acc l' :=
        idtac acc l';
        lazymatch n' with
        | 0 => go n l (@nil T) (acc ++ l')
        | S ?n'' =>
          lazymatch l with
          | nil => let x := constr:(acc ++ l') in
                  let res := eval cbv in x in res
          | ?x :: ?xs =>
            go n'' xs (x :: acc) l'
          end
        end in
    go n l (@nil T) (@nil T)
  end.

let x := gen_reverse1 2 (1 :: 2 :: 3 :: 4 :: 5 :: nil) in idtac x.
]]

<<
Error: Expression does not evaluate to a tactic.
>>

What's the problem here?
 *)

(** c.f. in %\cite[Section 14.3]{cpdt}%, in Ltac, side effectful tactics are in fact
carried by something called _tactic values_. When an expression is evaluated into a
tactic value, Coq will invoke some imperative mechanism to perform somde side
effectful behavior, like manipulating contexts or proof term; on the other hand, if
such thing does not happen, Ltac is in fact a purely functional programming language.

In above error message, what Coq does not like is, when it sees tactical [;], it
expects two expressions that would be evaluated to tactic values, and indeed [idtac]
will, but not the RHS, which was evaluated into a Gallina term.

While when CPS is used, all terms are tactic terms, and therefore Coq is consistently
happy, and Gallina terms are carried within Ltac closures.  *)
(* unhide *)
(* unhide *)

(** * Wrapping up Your Tactics *)

(* hide *)
(** Just like regular software development, there are separations between
"implementation details" and "APIs". As we've seen, Ltac programs are untyped, and
therefore if one tactic is too complicated, it's very easy to misuse, and it might
take a person long time to scratch his head to figure out what's wrong with the
proof. To tacle this, we should use [Tactic Notation] to wrap up the tactics.

[Tactic Notation] is pretty much the same as [Notation], but for tactics. It's very
nice to wrap things up in [Tactic Notation] in a form that ressembles the English
language. Another benefit of using [Tactic Notation] is, it's easier for code viewer,
as it's clear that [Tactic Notation] can only be APIs. Good [Tactic Notation]s can
improve the readability of the proof script by a lot. *)

Require Import Matches.

Tactic Notation "clear" "dup" :=
  find_dup_hyp ltac:(fun H H' X => first [clear H | clear H']) ltac:(idtac).

Tactic Notation "clear" "dups" := repeat clear dup.

Tactic Notation "unique" "context" :=
  find_dup_hyp ltac:(fun H H' _ => fail 1 H "and" H' "are the same!") ltac:(idtac).

Ltac same t1 t2 :=
  lazymatch t1 with
  | t2 => idtac
  | _ => fail
  end.

Tactic Notation "in" "context" constr(trm) :=
  match goal with
  | [ H : trm |- _ ] => idtac
  | _ => fail
  end.

Tactic Notation "dupped" hyp(H0) :=
  find_dup_hyp
    ltac:(fun H H' _ =>
            tryif first [same H H0 | same H H'] then fail 1 H0 "is dupped" else fail)
           ltac:(idtac).

Tactic Notation "contains" constr(trm) :=
  match goal with
  | [ H : context[trm] |- _ ] => idtac H
  end.

Goal False.
  do 2 pose proof I.
  do 2 pose proof (@eq_refl _ 0). pose proof (@eq_refl _ 1).
  Fail dupped H.

    (**
<<
Tactic failure: H is dupped.
>> *)
  
    in context True.
    contains @eq.
Abort.

(** However, there are strong limitations in [Tactic Notation], which gives some
troubles to write concise code on that part. *)

(** c.f. Chapter 12, %\cite{refman}%. *)
(* unhide *)