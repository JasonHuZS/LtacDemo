(** %\chapter{More Tactics}% *)

(** This chapter demonstrates some tactic development in a "serious" manner. Let's
consider more tactics. *)

Ltac find_hyp trm tac bas :=
  lazymatch goal with
  | [|- forall _, _] =>
    let x := fresh in
    intro x;
    lazymatch type of x with
    | context[trm] => tac x
    | _ => find_hyp trm tac bas
    end
  | _ => bas
  end.

Tactic Notation "induction" "on" constr(trm) :=
  find_hyp trm ltac:(fun x => induction x)
                      ltac:(fail "cannot find" trm "to perform induction on").

Tactic Notation "destruct" "on" constr(trm) :=
  find_hyp trm ltac:(fun x => destruct x)
                      ltac:(fail "cannot find" trm "to perform destruction on").

Goal forall n, 0 <= n.
Proof. induction on nat. Abort.

(** As you can see, you can avoid talking about lemma specific things if tactics are
wrapped up properly. *)

(** Another example is, please consider [inversion] tactic, which might be applied to
generate hypothesis which might contain more information, while it's not always the
case. What's worse, sometimes inversion might break cases meaninglessly. Therefore, we
want to make sure inversion only generate "useful" things that is not already existent
in the context. *)

Require Import Matches.
Require Import LtacProg.
Require Import Coq.Lists.List.
Import ListNotations.

(** Following tactic does precisely what the previous description says. *)

Ltac conservative_invert H :=
  let x := numgoals in
  inversion H;
  unique context;
  let y := numgoals in
  guard x >= y;
  subst;
  clear dups.

Ltac inverts :=
  let rec scan_ctx pred :=
      match goal with
      | [ H : ?T |- _ ] =>
        lazymatch type of T with
        | Prop =>
          pred H;
          try conservative_invert H;
          scan_ctx ltac:(fun H' => different H H'; pred H')
        end
      | _ => idtac
      end in
  clear dups; scan_ctx ltac:(fun _ => idtac).

Goal forall (a b c d : nat), (a, b) = (c, d) -> b = d.
Proof.
  intros. inverts. reflexivity.
Qed.

Goal forall (a b : nat) l,
    1 <= length l ->
    Forall (fun x => x = (a, b)) l ->
    match l with
    | x :: _ => x = (a, b)
    | _ => False
    end.
Proof.
  destruct on list; intros; inverts.
  reflexivity.
Qed.

(** Let's look into another solver that will utilize backtracking and behave like
[auto] (except it cannot inspect hint databases). *)

Section MyAuto.
  Variable A : Type.

  Ltac my_auto' fuel :=
    lazymatch fuel with
    | 0 => fail
    | S ?n =>
      cbn in *;
      inverts;
      try reflexivity;
      repeat
        (match goal with
         | [ H : _ |- _ ] =>
           exact H
         | [ H : forall _, _ |- _ ] =>
           eapply H;
           my_auto' n
           || match goal with
             | |- _ = _ =>
               symmetry;
               my_auto' n
             end
         end
         || f_equal);
      fail
    end.

  Ltac my_auto fuel := intros; repeat split; (my_auto' fuel || idtac).
  
  Goal forall (l : list A), l ++ nil = l.
  Proof. induction on list; my_auto 1. Qed.

  Goal forall (l1 l2 l3 : list A), l1 ++ l2 ++ l3 = (l1 ++ l2) ++ l3.
  Proof.
    induction on list; my_auto 1.
    Restart.
    induction on list.
    Info 1 my_auto 1.

    (**
<<
intros **;split
>> *)
    
    Info 1 my_auto 1.
    
    (**
<<
intros **;cbn in *;inverts ;f_equal ;<apply>
>> *)
    
  Qed.
  
  Goal forall (x y : list A) (a b : A), x ++ [a] = y ++ [b] -> x = y /\ a = b.
  Proof.
    induction on list; destruct on list; my_auto 2.

    (** We add a lemma to context to simulate hint database. *)
    
    all:pose proof app_cons_not_nil;
      exfalso; my_auto 2.

    Restart.

    pose proof app_cons_not_nil.

    (** Or more concisely, *)
    
    induction on list; destruct on list; my_auto 2; exfalso; my_auto 2.

  (** this sequence can be read as following: we perform induction and case analysis
    on two lists respectively, which was sufficient to prove some cases; the remaining
    cases are infeasible, which can also be proved subsequently. *)
    
  Qed.
  
End MyAuto.


Section GenTerm.
  
  (** The next example is to consider following isomorphism:
<<
forall x : T, P x /\ Q x <===>  (forall x : T, P x) /\ (forall x : T, Q x)
>>
   
   Let's manually expand terms of this form manually in recursion. Note that following
   method doesn't quite seem to work. *)

  Set Implicit Arguments.
  
  Theorem iso: forall (T : Type) (P Q : T -> Prop),
      (forall x, P x /\ Q x) <-> (forall x, P x) /\ forall x, Q x.
  Proof. firstorder. Qed.

  (** I cheated here by using [firstorder] tactic, which can generally be used to
  solve goals that can be solved within first order logic. *)

  Ltac expand H := repeat rewrite iso in H.

  Hypothesis T : Type.
  Hypothesis M N P Q : T -> Prop.
  Hypothesis R S : Prop.
  
  Goal (forall x, R -> S -> P x /\ Q x) -> False.
  Proof.
    intro H. expand H.

  (** The goal is unchanged. The reason is implication is right associative, and
    therefore the form does not match. *)

  Abort.

  (** Let's consider a tactic that will keep walking to the right of the (dependent)
  implication and build simpler terms using this isomorphism. We will be using lots of
  the previously mentioned methods and bring them together into a single
  tactic. Additionally, following tactic also implement another isomorphism:

<<
P \/ Q -> X <===> P -> X /\ Q -> X
>>

   Note that this isomorphism has no chance to be dependent, so it's easier in
   nature.*)

  Ltac build_trm trm :=
    lazymatch trm with
    | forall x : ?A, @?X x /\ @?Y x =>
      let trm1 := eval cbn beta in (forall x : A, X x) in
      let trm2 := eval cbn beta in (forall x : A, Y x) in
      let trm1' := build_trm trm1 in
      let trm2' := build_trm trm2 in
      constr:(trm1' /\ trm2')
    | ?A \/ ?B -> ?X =>
      let trm1 := build_trm (A -> X) in
      let trm2 := build_trm (B -> X) in
      constr:(trm1 /\ trm2)
    | forall x : ?A, @?B x =>
      lazymatch B with
      | fun y : A => ?body =>
        let proxy := fresh in
        let result := constr:(fun y : A =>
                             match body with
                             | proxy =>
                               ltac:(let form := eval cbv delta [proxy] in proxy in
                                     let nf := build_trm form in
                                     exact nf)
                             end) in
        let cl := eval cbn beta in (forall x : A, result x) in
        match cl with
        | trm => trm
        | _ => build_trm cl
        end
      end
    | _ => trm
    end.

  Require Import Coq.Program.Tactics.
  
  Goal (forall x, (R \/ S -> M x /\ (N x /\ P x) /\ Q x) /\ (S -> forall y, P y /\ Q y)) -> forall x, R -> N x.
  Proof.
    intro H.
    let tH := type of H in
    let y := build_trm tH in
    assert y by firstorder.

    (** We can expand the cases to see the conjunctions more clearly. *)

    destruct_pairs.
    
    (** As we can see, indeed we generate the proper term by recursively going to the
    right of the targeted implication. This tactic is quite useful when preparing
    proof context for [auto]/[eauto], because these two tactics are based on the exact
    shape of the goal and perform backward reasoning. If the goal matches one of the
    conjunctions hidden inside of a (dependent) implication, it will not be
    applied. Now we know [auto] is sufficient to handle this goal. *)

    auto 1.

    Restart.
    intros; auto.
    
  (** As described, it won't work if the context is not ready. However, the pattern
    for inspecting the right of an dependent implication can be unnecessarily too
    complicated as of current state.*)

    firstorder.

    (** Coq actually has known this fact. *)
    
  Qed.
  
End GenTerm.