(** %\chapter{Pattern Matching And Backtracking}% *)

(** This chapter is meant to complement the detailed discussion on behavioral
differences between different [match]es, as I found that it's critical to understand
backtracking and begin writing non-trivial programs in Ltac. *)

(** * Pattern Matchings *)

(** Following code will demonstrate various behaviors of different pattern matchings
by proving the simplest theorem. *)

Goal True.
Proof.

  (** We can prove it by directly applying the constructor. *)

  apply I.

  (* hide *)
  (* hide *)
  Restart.

  (** Let's look at the most frequently used, [match]. *)

  match goal with
  | _ => fail
  | _ => apply I
  end.
  (* unhide *)
  (* hide *)
  
  (** This match was successful, because the [fail] inside triggerd a backtracking
  of [match] so that it falls back to the second case. We can see it from
  debugger. *)

  Restart.

  (** [Set Ltac Debug.] *)

  match goal with
  | _ => fail
  | _ => apply I
  end.

  (** [Unset Ltac Debug.] *)

  (* unhide *)
  (* hide *)
  (** In fact, the backtracking is more subtle, since the backtracking point is to the
  RHS of the match. To illistrate that, we should see it from side effect. Hence,
  let's introduce something to the context in order to observe that.  *)

  Restart.
  pose proof (@eq_refl _ 0).
  pose proof (@eq_refl _ 1).

  match goal with
  | [H : ?n = _ |- _ ] =>
    let x := type of H in
    idtac H x;
      match n with
      | 1 => fail 1
      | _ => idtac
      end
  | _ => fail "not reachable."
  end.
  (* unhide *)
  (* hide *)

  (** There are three points here.

   1. [match] behaves deterministically, by scanning hypothesis from bottom to top on
   the screen. If one wants the other order, he would need [match reverse goal with];

   2. Given that, we know we must see [1 = 1] first. then we intentionally [fail] the
   search. Notice that we also pass in the fail level [1], which asks Coq to also fail
   the outer RHS;

   3. Then, Coq must trigger backtracking. However, since the backtracking point is on
   the RHS of [match], Coq picks another suitable option for the SAME condition, and
   run the RHS again. In this case, it falls to [idtac], which leads the tactic to be
   successful.

   To conclude, [match] backtracks on both the RHS and the conditions. *)

  (** As you can see, Coq tries to be helpful, by exploring many options and stops at
  the first match that satisfies the condition. However, this also increases the
  complexity of the search, at least when you don't expect correctly. *)
  (* unhide *)
  (* hide *)
  
  (** One variation of [match] is [lazymatch], which is [match], except does not
  perform backtracking at all. *)

  Fail lazymatch goal with
       | [H : ?n = _ |- _ ] =>
         let x := type of H in
         idtac H x;
           match n with
           | 1 => fail 1
           | _ => idtac "not reachable."
           end
       | _ => idtac "not reachable."
       end.

  (** It will give following message:
<<
H0 (1 = 1)
The command has indeed failed with message:
         Tactic failure.
>> *)

  (** [lazymatch] does not introduce any backtracking point. To some degree,
  [lazymatch] in fact is closest to what we would understand as pattern matching in
  normal programming context.

   However, this also limits how much proof search it can do. On the other hand, it's
   very helpful when we are writing functional programs in Ltac, where sometimes no
   backtracking is expected. *)

Abort.

Ltac different t1 t2 :=
  lazymatch t1 with
  | t2 => fail
  | _ => idtac
  end.

Ltac find_dup_hyp tac non :=
  match goal with
  | [ H : ?X, H' : ?X |- _ ] =>
    lazymatch type of X with
    | Prop => 
      tac H H' X
    | _ => idtac
    end
  | _ => non
  end.

Local Ltac clear_dup :=
  find_dup_hyp ltac:(fun H H' X => first [clear H | clear H']) ltac:(idtac).

Local Ltac clear_dups := repeat clear_dup.

Local Ltac uniq_ctx :=
  find_dup_hyp ltac:(fun H H' _ => fail 1 H "and" H' "are the same!") ltac:(idtac).

Goal True.
Proof.
  do 2 pose proof I.
  
  (**
  [uniq_ctx.]
<<
Tactic failure: H and H0 are the same!.
>> *)
  clear_dups.
  (* unhide *)
  (* hide *)
  
  (** Another kind of [match] is [multimatch], which is kind of a dual of [lazymatch]:
  it allows subsequent tactic backtracks into its RHS. *)

  multimatch goal with
  | _ => idtac
  | _ => apply I
  end;
    fail.

  (* unhide *)
  (* hide *)
  (** This tactic succeeded, because [fail] triggered a backtracking on the RHS
  (namely [idtac]), which falls to the second branch and [apply I] discharged the
  goal.

   [multimatch] has almost too strong backtracking power, which makes it very hard to
   use. One might almost always want to use additional tactical to limit this
   backtracking behavior so that the complexity of seaching does not explode
   dramatically. *)
  
  Restart.
  Fail once (multimatch goal with
             | _ => idtac
             | _ => apply I
             end; idtac);
    fail "does not backtrack into multimatch above".

  (** The following two uses of tacticals [||] and [+] also following the same
  principle. *)

  Restart.
  fail || apply I.
  Restart.
  (idtac + apply I); fail.
  (* unhide *)
Qed.
(* unhide *)

(** * Counting Backtracking Points *)

(* hide *)
(** The previous code has demonstrated how important it is to mentally keep track of
backtracking points. In some situation, we want to make certain tactics fail, in that
situation, we will need to invoke [fail] tactical. Then, it's very important to count
the failure level correctly, especially in a recursive function/tactic. *)

(** Following is an easy auto backward reasoning tactic to solve a chain of
non-dependent implications. *)

Local Ltac can_prove_false H :=
  lazymatch H with
  | False => idtac
  | _ -> ?R => can_prove_false R
  end.

(** To illustrate how failure level impact tactic, we leave a [lvl] argument and the
most suitable [lvl] shall be 2. *)

Local Ltac my_search lvl :=
  match goal with
  | [H : ?X |- ?X] =>
    idtac "exact" X;
    exact H
  | [H : _ -> _ |- _ ] =>
    let tH := type of H in
    idtac "about to apply" H "of type" tH;
    apply H;
    let G := match goal with |- ?G => G end in
    idtac "applied" H ":" tH ", generated goal" G;
    match goal with
    | [ |- False] =>
      match goal with
      | [H' : _ -> _ |- _ ] =>
        let tH' := type of H' in
        can_prove_false tH';
        idtac "found proof for falsehood:" tH';
        my_search lvl
      | _ =>
        idtac "cannot find proof for falsehood";
        fail lvl
      end
    | _ => my_search lvl
    end
  end.

(* hide *)
Section FailDemo.

  Local Ltac two := 2.
  
  Hypothesis P Q R S : Prop.

  Goal (P -> Q) -> (Q -> R) -> P -> R.
  Proof.
    intros. my_search two.
  Qed.

  Goal (P -> Q) -> (Q -> False) -> (False -> R) -> P -> R.
  Proof.
    intros. my_search two.
  Qed.

  Goal (False -> P) -> P.
  Proof.
    Fail my_search two.
  Abort.
  
  Goal Q -> (Q -> P) -> (False -> P) -> P.
  Proof.
    intros.
    Fail my_search ltac:(3).
    (** 3 is the exact level of failure, because of 3 layers of [match]es. Therefore
    it fails the goal. *)
    
    my_search two.
  Qed.
  
  (* hide *)
  Goal P -> (P -> S) -> (False -> S) -> (S -> R) -> R.
  Proof.
    intros.
    Fail my_search ltac:(100).

    (** The failure level decreases as recursion goes deeper. Hence it's very
    important to control the tactic where it fails at a right spot where no more
    unexpected search is performed afterwards.
<<
The command has indeed failed with message:
In nested Ltac calls to "my_search" and "my_search", last call failed.
Tactic failure (level 95).
>> *)
    
    my_search ltac:(1).
    Restart.
    intros. my_search two.

  (** From the output, we can tell even if passing in [1] also succeeded, but it
    does more search than [2]. *)
    
  Qed.

  (** One might ask, "why borther? There are cases wherewe can just pass in a massive
  failure level, such that intended failure can always get through." The answer is, it
  does not matter only if you actually never want your tactics to fail. In an
  automated proof, tactics almost always fail and therefore counting failure level is
  necessary in order to maintain an efficient proof flow. *)
  (* unhide *)
  
End FailDemo.
(* unhide *)
(* unhide *)

(** * Some Quirks on Matches *)

(* hide *)
(** There are some quirks in Ltac where the error messages are very unreasonable. *)

Section Quirks.

  Variable A : Type.
  Hypothesis P : forall T : Type, T -> Prop.

  Goal (forall x : A, P A x) -> exists x, P A x.
  Proof.
    intros.

    (** Should the below tactic print "yes" or fail? *)

    match goal with
    | [H : forall _ : A, _ |- _] => idtac "yes"
    end.

    (* hide *)
    (** And yes it does, but what about the following? *)

    Fail match goal with
         | [H : forall _ : A, ?P |- _] => idtac "yes"
         end.

    (** The above gives following message:
<<
The command has indeed failed with message:
Error: No matching clauses for match.
>> 

     Why is that? *)

    (** The reason is, Coq prohibits capturing of dependent RHS where there are
    variables appearing free (c.f. https://github.com/coq/coq/issues/6243) (also
    %\cite[Section 15.5]{cpdt}%). However, there is a way to get around it, by
    eta-expansion: *)

    match goal with
    | [H : forall x : A, @?P x |- _] =>
      idtac P;
        match P with
        | fun x : _ => ?P' =>
          idtac P'
      end
    end.

    (** It uses eta-expansion to convert the dependent implication into a value level
    term, which then can be broken down by another match, then hence open to further
    recursion. *)

    Abort.
    (* unhide *)

  (* hide *)
  (** Another quirk is when pass in leading [match] expression as a thunk to other
  tactic. %\footnote{Please also see my answer from this email thread:
  \url{https://sympa.inria.fr/sympa/arc/coq-club/2018-04/msg00076.html}}% *)

  Ltac intro_id tac := intros; tac.

  Hypothesis Q : Prop.

  Goal Q -> Q.
  Proof.
    Fail intro_id ltac:(match goal with | [H : Q |- Q] => exact H end).

    (**
<<
Error: No matching clauses for match.
>> 

     This indeed is a weird limitation. However, there is a workaround. *)

    intro_id ltac:(idtac; match goal with | [H : Q |- Q] => exact H end).

    (** It seems that thunk just doesn't like to begin with a [match]. *)

  Qed.
  (* unhide *)
End Quirks.
(* unhide *)