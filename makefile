# modified from CPDT's makefile
MODULES := Matches LtacProg LowLevel MoreTactics
VS      := $(MODULES:%=src/%.v)
TEX     := $(MODULES:%=latex/%.v.tex)
COQDOC  := coqdoc

.PHONY: coq clean doc

coq: makefile.coq
	$(MAKE) -f makefile.coq

makefile.coq: makefile $(VS)
	coq_makefile $(VS) COQC = 'coqc -R src ""' \
		COQDEP = 'coqdep -R src ""' -o makefile.coq

clean: makefile.coq
	$(MAKE) -f makefile.coq clean
	rm -f makefile.coq .depend
	cd latex; rm -f *.log *.aux *.dvi *.v.tex *.toc *.bbl *.blg *.idx *.ilg *.ind *.out

doc: latex/comp.pdf latex/pre.pdf

latex/%.v.tex: makefile src/%.v src/%.glob
	cd src ; $(COQDOC) --interpolate --latex --body-only -s \
		$*.v -o ../latex/$*.v.tex

latex/comp.pdf: latex/comp.tex $(TEX) latex/ref.bib
	cd latex ; pdflatex comp ; pdflatex comp ; biber comp ; makeindex comp ; pdflatex comp ; pdflatex comp

latex/pre.pdf : latex/pre.tex latex/ref.bib
	cd latex ; pdflatex pre ; biber pre ; pdflatex pre
